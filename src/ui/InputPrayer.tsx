import React from 'react'
import {StyleSheet, TextInput} from 'react-native'
import {variables} from '../assets/variables'

type Props = {
  input: {}
  placeholder: string,
  secure?: boolean
}

export const InputPrayer = ({input, placeholder, secure}: Props) => {
  return (
    <TextInput
      placeholder={placeholder}
      {...input}
      secureTextEntry={secure}
      style={styles.input}
    />
  )
}

const styles = StyleSheet.create({
  input: {
    width: 209,
    height: 30,
    marginTop: 10,
    marginBottom: 10,
    paddingStart: 5,
    backgroundColor: variables.colors.defaultBG,
    fontSize: variables.sizing.title,
  },
})
