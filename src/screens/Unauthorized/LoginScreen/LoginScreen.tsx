import React from 'react'
import {Form, Field} from 'react-final-form'
import {Text, View, StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native'
import {useNavigation} from '@react-navigation/core'
import {useDispatch} from 'react-redux'
import {Input} from '../../../ui/Input'
import {signIn} from '../../../redux/user/actions'
import {ERoutes} from '../../../constants'
import {variables} from '../../../assets/variables'

type ValuesType = {
  email: string
  password: string
}

export const LoginScreen = () => {
  const navigation = useNavigation()
  const dispatch = useDispatch()

  const onSubmit = async (values: ValuesType) => {
    const data = {
      email: values.email,
      password: values.password,
    }
    try {
      await dispatch(signIn(data))
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text>Log in or register</Text>
      <Form
        onSubmit={onSubmit}
        render={({handleSubmit}) => (
          <View>
              {/*@ts-ignore*/}
              <Field component={Input}
                     name="email"
                     placeholder="Your email ..."
              />
              {/*@ts-ignore*/}
              <Field component={Input}
                     name="password"
                     placeholder="Your password ..."
                     secure
              />
            <TouchableOpacity style={styles.button} onPress={handleSubmit}>
              <Text>LOG IN</Text>
            </TouchableOpacity>
          </View>
        )}
      />
      <View>
        <TouchableOpacity
          style={styles.button}
          //@ts-ignore
          onPress={() => navigation.navigate(ERoutes.ROUTE_REGISTER)}
        >
          <Text>Go to registration</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: variables.colors.theme,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: 209,
    height: 30,
    marginBottom: 10,
    backgroundColor: variables.colors.defaultBG,
    color: variables.colors.defaultBlack,
    borderWidth: 1,
    borderColor: variables.colors.defaultBlack,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

