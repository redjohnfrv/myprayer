import {AxiosPromise} from 'axios'
import {globalApiHttp} from '../../api/api'
import {UserSignInType, UserSignUpType, UserType} from './types'
import {URL_PREFIX_SIGN_IN, URL_PREFIX_SIGN_UP} from '../../constants'

export const signUpApi = (payload: UserSignUpType): AxiosPromise<UserType> => {
  return globalApiHttp.post(URL_PREFIX_SIGN_UP, payload)
}

export const signInApi = (payload: UserSignInType): AxiosPromise<UserType> => {
  return globalApiHttp.post(URL_PREFIX_SIGN_IN, payload)
}
