import React from 'react'
import {InputPrayer} from '../../../../ui/InputPrayer'
import {Form, Field} from 'react-final-form'
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  View } from 'react-native'
import {variables} from '../../../../assets/variables'
import {images} from '../../../../assets/images'

type TitleType = {
  title: string
}
type Props = {
  onSubmit: ({title}: TitleType) => void
}

export const PrayerAdd = ({onSubmit}: Props) => {
  return (
    <View style={styles.container}>
      <Form
        onSubmit={onSubmit}
        render={({handleSubmit}) => (
          <View style={styles.addWrapper}>
            <TouchableOpacity style={styles.addButton} onPress={() => handleSubmit()}>
              <Image source={images.addPray} />
            </TouchableOpacity>
            {/*@ts-ignore*/}
            <Field component={InputPrayer}
                   name="title"
                   placeholder="Add a prayer..."
            />
          </View>
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    margin: 15,
    alignSelf: 'stretch',
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: variables.colors.defaultBorder,
    borderRadius: 10,
  },
  addWrapper: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  addButton: {
    width: 24,
    height: 24,
    marginLeft: 14,
  },
})
