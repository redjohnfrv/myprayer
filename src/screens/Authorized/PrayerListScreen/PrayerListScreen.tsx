import React, {useState} from 'react'
import {RootState} from '../../../redux/store'
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native'
import {useSelector, useDispatch} from 'react-redux'
import {
  selectCheckedPrayersByColumn,
  selectPrayersByColumn,
  selectUncheckedPrayersByColumn,
} from '../../../redux/prayers/selectors'
import {selectColumn} from '../../../redux/columns/selectors'
import {Image, View, SafeAreaView} from 'react-native'
import {useNavigation} from '@react-navigation/core'
import {PrayerAdd} from './PrayerAdd/PrayerAdd'
import {PrayerItem} from './PrayerItem/PrayerItem'
import {
  EMPTY_STRING,
  HIDE_ANSWERED,
  SHOW_ANSWERED,
  TAB_SWITCH_LEFT
} from '../../../constants'
import {addPrayer} from '../../../redux/prayers/actions'
import {variables} from '../../../assets/variables'
import {images} from '../../../assets/images'

type Props = {
  route: any,
}

export const PrayerListScreen = (props: Props) => {
  const {id} = props.route.params

  const dispatch = useDispatch()
  const navigation = useNavigation()

  const title = useSelector((state: RootState) => {
   return selectColumn(state, id)!.title
  })!

  const cardsIds = useSelector((state: RootState) =>
    selectPrayersByColumn(state, id),
  )!

  const cardsAnswered = useSelector((state: RootState) =>
    selectCheckedPrayersByColumn(state, id),
  )!
  const cardsUnanswered = useSelector((state: RootState) =>
    selectUncheckedPrayersByColumn(state, id),
  )!

  const [activeTab, setActiveTab] = useState(true)

  const tabSwitcher = (tab: string) => {
    tab === TAB_SWITCH_LEFT
      ? setActiveTab(true)
      : setActiveTab(false)
  }

  const [btnPushed, setBtnPushed] = useState(false)
  const [btnTitle, setBtnTitle] = useState(SHOW_ANSWERED)
  const handleBtnPush = () => {
    switch (btnPushed) {
      case false:
        setBtnPushed(true)
        setBtnTitle(HIDE_ANSWERED)
        break;
      case true:
        setBtnPushed(false)
        setBtnTitle(SHOW_ANSWERED)
    }
  }

  const onSubmit = async ({title}: {title: string}) => {
    try {
      await dispatch(
        addPrayer({
          title,
          description: EMPTY_STRING,
          checked: false,
          columnId: id,
        }),
      )
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={styles.arrow} onPress={() => navigation.goBack()}>
          <Image style={styles.arrowImg} source={images.backArrow} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.settings}>
          <Image source={images.settings} />
        </TouchableOpacity>
        <View style={styles.title}>
          <Text style={styles.titleText}>{title}</Text>
        </View>
        <View style={styles.tabMenu}>
          <TouchableOpacity
            //@ts-ignore
            style={tabStyleHandler(activeTab)}
            onPress={() => tabSwitcher(TAB_SWITCH_LEFT)}
          >
            <Text
              //@ts-ignore
              style={tabTextStyleHandler(activeTab)}
            >my prayers</Text>
          </TouchableOpacity>
          <TouchableOpacity
            //@ts-ignore
            style={tabStyleHandler(!activeTab)}
            onPress={() => tabSwitcher(EMPTY_STRING)}
          >
            <View style={styles.tabBody}>
              <Text
                //@ts-ignore
                style={tabTextStyleHandler(!activeTab)}
              >subscribed</Text>
              <View style={styles.subscribe}>
                <Text style={styles.subscribeText}>3</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      {activeTab && <PrayerAdd onSubmit={onSubmit} />}
      {activeTab ? (
        <SafeAreaView>
          {btnPushed ? (
            <View>
              <FlatList
                //@ts-ignore
                style={styles.notAnswer}
                data={cardsUnanswered}
                renderItem={(item) => <PrayerItem id={item.item} />}
                keyExtractor={item => String(item)}
              />
              <TouchableOpacity style={styles.button} onPress={() => handleBtnPush()}>
                <Text style={styles.buttonText}>{btnTitle}</Text>
              </TouchableOpacity>
              <FlatList
                data={cardsAnswered}
                renderItem={(item) => <PrayerItem id={item.item} />}
                keyExtractor={item => String(item)}
              />
            </View>
          ) : (
            <SafeAreaView>
              <FlatList
                data={cardsIds}
                renderItem={(item) => <PrayerItem id={item.item} />}
                keyExtractor={item => item + '1'}
              />
              <TouchableOpacity style={styles.button} onPress={() => handleBtnPush()}>
                <Text style={styles.buttonText}>{btnTitle}</Text>
              </TouchableOpacity>
            </SafeAreaView>
          )}
        </SafeAreaView>
      ) : null}
    </SafeAreaView>
  )
}

function tabStyleHandler(tab: boolean) {
  let bg
  tab
    ? bg = variables.colors.defaultBlue
    : bg = variables.colors.defaultBorder
  return {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: bg
  }
}
function tabTextStyleHandler(tab: boolean) {
  let bg
  tab
    ? bg = variables.colors.defaultBlue
    : bg = variables.colors.defaultBorder
  return {
    textTransform: 'uppercase',
    color: bg,
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    marginTop: 50,
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
  },
  header: {
    width: '100%',
    height: 100,
    borderBottomWidth: 1,
    borderBottomColor: variables.colors.defaultBorder,
  },
  arrow: {
    position: 'absolute',
    top: 20,
    left: 20,
    zIndex: 10,
  },
  arrowImg: {
    width: 20,
    height: 20,
  },
  notAnswer: {
    textDecorationLine: 'underline',
  },
  settings: {
    position: 'absolute',
    top: 20,
    left: 345,
  },
  title: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    textAlign: 'center',
    fontSize: variables.sizing.title,
    lineHeight: 20,
    fontWeight: 'bold',
    color: variables.colors.defaultBlack,
  },
  tabMenu: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-evenly',
  },
  tab: {
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: variables.colors.defaultBlue
  },
  tabBody: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  subscribe: {
    width: 16,
    height: 16,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 4,
    backgroundColor: variables.colors.defaultRed,
  },
  subscribeText: {
    fontSize: 9,
    color: '#FFF',
  },
  button: {
    width: 209,
    height: 30,
    marginTop: 21,
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: variables.colors.theme,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    textTransform: 'uppercase',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFF',
  },
})
