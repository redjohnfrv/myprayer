import {createSelector} from 'reselect'
import {RootState} from '../store'

export const selectMembersIds = (state: RootState) =>
  state.members.map(member => member.id)

export const selectMember = createSelector(
  (state: RootState) => state.members,
  (state: RootState, id: number) => id,
  (members, id) => members.find(member => member.id === id),
)

export const selectMembersByPrayer = createSelector(
  (state: RootState) => state.members,
  (state: RootState, prayerId: number) => prayerId,
  (members, prayerId) =>
    members
      .filter(member => member.prayerId === prayerId)
      .map(member => member.id),
)
