import backArrow from './images/back.png'
import backArrowWhite from './images/backWhite.png'
import addPlus from './images/add.png'
import addWhitePlus from './images/addWhite.png'
import closeBtn from './images/close.png'
import settings from './images/settings.png'
import lineBlue from './images/lineBlue.png'
import lineGrey from './images/lineGrey.png'
import lineRed from './images/lineRed.png'
import prayBlue from './images/prayBlue.png'
import people from './images/people.png'
import addPray from './images/addPray.png'
import prayerLineWhite from './images/prayerLineWhite.png'
import addComment from './images/addComment.png'

export const images = {
  backArrow,
  backArrowWhite,
  addPlus,
  addWhitePlus,
  addPray,
  addComment,
  closeBtn,
  settings,
  lineBlue,
  lineGrey,
  lineRed,
  prayBlue,
  people,
  prayerLineWhite,
}
