import {createSelector} from 'reselect'
import {RootState} from '../store'

export const selectColumns = (state: RootState) =>
  state.columns.map(column => column.id)

export const selectColumn = createSelector(
  (state: RootState) => state.columns,
  (state: RootState, id: number) => id,
  (columns, id) => columns.find(column => column.id === id),
)
