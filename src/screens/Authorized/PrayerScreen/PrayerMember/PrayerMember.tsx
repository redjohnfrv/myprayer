import React from 'react'
import {StyleSheet, View} from 'react-native'
import {variables} from '../../../../assets/variables'

type Props = {
  id: number
}

export const PrayerMember = ({id}: Props) => {
  return(
    <View style={styles.container} />
  )
}

const styles = StyleSheet.create({
  container: {
    width: 32,
    height: 32,
    marginRight: 12,
    borderRadius: 16,
    backgroundColor: variables.colors.defaultBlue,
  },
})
