import {createSelector} from 'reselect'
import {RootState} from '../store'

export const selectCommentsIds = (state: RootState) =>
  state.comments.map(comment => comment.id)

export const selectComment = createSelector(
  (state: RootState) => state.comments,
  (state: RootState, id: number) => id,
  (comments, id) => comments.find(comment => comment.id === id),
)

export const selectCommentsByPrayer = createSelector(
  (state: RootState) => state.comments,
  (state: RootState, prayerId: number) => prayerId,
  (comments, prayerId) =>
    comments
      .filter(comment => comment.prayerId === prayerId)
      .map(comment => comment.id),
)
