import React  from 'react'
import {StyleSheet, TextInput} from 'react-native'
import {variables} from '../assets/variables'

type Props = {
  input: {}
  placeholder: string
  secure?: boolean
}

export const InputTransparent = ({input, placeholder, secure}: Props) => {
  return (
    <TextInput
      placeholder={placeholder}
      {...input}
      secureTextEntry={secure}
      style={styles.input}
    />
  )
}

const styles = StyleSheet.create({
  input: {
    width: 209,
    height: 30,
    marginTop: 10,
    marginBottom: 10,
    paddingStart: 5,
    borderWidth: 0,
    fontSize: 17,
    color: variables.colors.defaultLightGrey,
  },
})
