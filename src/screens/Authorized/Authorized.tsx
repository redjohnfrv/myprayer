import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import {DeskScreen} from './DeskScreen/DeskScreen'
import {PrayerListScreen} from './PrayerListScreen/PrayerListScreen'
import {PrayerScreen} from './PrayerScreen/PrayerScreen'
import {ERoutes} from '../../constants'

export const Authorized = () => {
  const Stack = createStackNavigator()

  return (
    <Stack.Navigator initialRouteName={ERoutes.ROUTE_DESK}>
      <Stack.Screen
        name={ERoutes.ROUTE_DESK}
        component={DeskScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={ERoutes.ROUTE_PRAYER_LIST}
        component={PrayerListScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={ERoutes.ROUTE_PRAYER_INFO}
        component={PrayerScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  )
}
