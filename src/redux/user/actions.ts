import {createAction, createAsyncThunk} from '@reduxjs/toolkit'
import {signUpApi, signInApi} from './api'
import {ParamsType, SignInParamsType, UserType} from './types'
import {
  ERROR_STRING,
  USER_SET,
  USER_SIGN_IN,
  USER_SIGN_UP
} from '../../constants'

export const setUser = createAction<any>(USER_SET)

export const signUp = createAsyncThunk<any, ParamsType>(
  USER_SIGN_UP,
  async params => {
    const {data} = await signUpApi(params)
    if (data.message) {
      throw new Error(ERROR_STRING)
    }
    return data
  },
)

export const signIn = createAsyncThunk<UserType, SignInParamsType>(
  USER_SIGN_IN,
  async params => {
    const {data} = await signInApi(params)
    if (data.message) {
      throw new Error(ERROR_STRING)
    }
    return data
  },
)
