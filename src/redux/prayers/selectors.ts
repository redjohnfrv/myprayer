import {createSelector} from 'reselect'
import {RootState} from '../store'

export const selectPrayer = createSelector(
  (state: RootState) => state.prayers,
  (state: RootState, id: number) => id,
  (prayers, id) => prayers.find(prayer => prayer.id === id),
)

export const selectPrayersByColumn = createSelector(
  (state: RootState) => state.prayers,
  (state: RootState, columnId: number) => columnId,
  (prayers, columnId) => prayers
    .filter(prayer => prayer.columnId === columnId)
    .map(prayer => prayer.id),
)

export const selectCheckedPrayersByColumn = createSelector(
  (state: RootState) => state.prayers,
  (state: RootState, columnId: number) => columnId,
  (prayers, columnId) => prayers
      .filter(prayer => prayer.columnId === columnId && prayer.checked === true)
      .map(prayer => prayer.id),
)

export const selectUncheckedPrayersByColumn = createSelector(
  (state: RootState) => state.prayers,
  (state: RootState, columnId: number) => columnId,
  (prayers, columnId) => prayers
      .filter(prayer => prayer.columnId === columnId && prayer.checked === false)
      .map(prayer => prayer.id),
)
