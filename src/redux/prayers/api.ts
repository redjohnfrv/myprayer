import {AxiosPromise} from 'axios'
import {globalApiHttp} from '../../api/api'
import {AddPrayerType, PrayerType, UpdatePrayerType} from './types'
import {URL_PREFIX_PRAYERS} from '../../constants'

export const getPrayersApi = (): AxiosPromise<PrayerType> => {
  return globalApiHttp.get(URL_PREFIX_PRAYERS)
}

export const addPrayerApi = (
  payload: AddPrayerType,
): AxiosPromise<PrayerType> => {
  const {columnId, ...body} = payload
  return globalApiHttp.post(`/columns/${String(columnId)}/prayers`, body)
}

export const updatePrayerApi = (
  payload: UpdatePrayerType,
): AxiosPromise<PrayerType> => {
  const {prayerId, ...body} = payload
  return globalApiHttp.put(`/prayers/${String(prayerId)}`, body)
}
