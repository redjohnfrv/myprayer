export const variables = {
  colors: {
    theme: '#BFB393',
    defaultBlue: '#72A8BC',
    defaultBlack: '#514D47',
    defaultRed: '#AC5253',
    defaultBG: '#FFF',
    defaultBorder: '#E5E5E5',
    defaultLightGrey: '#9C9C9C',
  },
  sizing: {
    title: 17,
  },
}
