import {createAsyncThunk} from '@reduxjs/toolkit'
import {getColumnsApi, addColumnApi} from './api'
import {ColumnType} from './types'
import {COLUMN_ADD, COLUMNS_GET, ERROR_STRING} from '../../constants'

type Params = {
  title: string
  description: string | null
}

export const getColumns = createAsyncThunk<ColumnType>(
  COLUMNS_GET,
  async () => {
    const {data} = await getColumnsApi()
    if (data.message) {
      throw new Error(ERROR_STRING)
    }
    return data
  },
)

export const addColumn = createAsyncThunk<ColumnType, Params>(
  COLUMN_ADD,
  async params => {
    const {data} = await addColumnApi(params)
    if (data.message) {
      throw new Error(`${ERROR_STRING}: ${data.message}`)
    }
    return data
  },
)
