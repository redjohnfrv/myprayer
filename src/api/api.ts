import {API_URL, TIMEOUT} from '../constants'
import Axios, {AxiosInstance} from 'axios'

export class Http {
  constructor(private readonly _axios: AxiosInstance) {}
  setAuthorizationHeader(token: string) {
    //@ts-ignore
    this._axios.defaults.headers.Authorization = `Bearer ${token}`
  }

  get get() {return this._axios.get}
  get post() {return this._axios.post}
  get put() {return this._axios.put}
  get patch() {return this._axios.patch}
  get delete() {return this._axios.delete}
  get request() {return this._axios.request}
}

export const globalApiHttp = new Http(
  Axios.create({
    baseURL: API_URL,
    withCredentials: false,
    timeout: TIMEOUT,
  }),
)
