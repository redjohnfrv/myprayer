import {takeEvery, put} from 'redux-saga/effects'
import {MEMBERS_UPDATE, MEMBER_ADD} from '../../constants'
import {PayloadAction} from '@reduxjs/toolkit'


function* workerSaga(action: PayloadAction<{id: number, prayerId: number}>) {
  const {id, prayerId} = action.payload
  console.log('saga works')
  yield put({type: MEMBERS_UPDATE, payload: {id, prayerId}})
}

function* watcherSaga() {
  yield takeEvery(MEMBER_ADD, workerSaga)
}

export default function* rootSaga() {
  yield watcherSaga()
}
