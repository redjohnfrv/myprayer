import {AxiosPromise} from 'axios'
import {globalApiHttp} from '../../api/api'
import {ColumnType, addColumnType} from './types'
import {URL_PREFIX_COLUMNS} from '../../constants'

export const getColumnsApi = (): AxiosPromise<ColumnType> => {
  return globalApiHttp.get(URL_PREFIX_COLUMNS)
}

export const addColumnApi = (
  payload: addColumnType,
): AxiosPromise<ColumnType> => {
  return globalApiHttp.post(URL_PREFIX_COLUMNS, payload)
}
