export const nothing = () => {
  console.log('nothing')
}
//
// import store from '../redux/store'
// import {userSelectors} from '../redux/user/selectors'
//
// const baseUrl = 'https://prayer.herokuapp.com'
//
// export const userApi = {
//   signup: (data: { email: string, name: string, password: string }) => axios({
//     method: 'POST',
//     url: `${baseUrl}/auth/sign-up`,
//     data,
//   }),
//   login: (data: { email: string, password: string }) => axios({
//     method: 'POST',
//     url: `${baseUrl}/auth/sign-in`,
//     data,
//   }),
// };
//
// export const columnsApi = {
//   getAllColumns: () => {
//     const token = userSelectors.getToken(store.getState());
//     return axios({
//       method: 'GET',
//       url: `${baseUrl}/columns`,
//       headers: { 'Authorization': `Bearer ${token}` },
//     });
//   },
// };
//
// export const prayersApi = {
//   getAllPrayers: () => {
//     const token = userSelectors.getToken(store.getState());
//     return axios({
//       method: 'GET',
//       url: `${baseUrl}/prayers`,
//       headers: { 'Authorization': `Bearer ${token}` },
//     });
//   },
// }
