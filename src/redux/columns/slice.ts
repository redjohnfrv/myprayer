import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {getColumns, addColumn} from './actions'
import {ColumnType} from './types'

const initialState = [] as ColumnType[]

const columnsSlice = createSlice({
  name: 'columns',
  initialState,
  reducers: {},
  extraReducers: {
    [getColumns.fulfilled.type]: (
      state,
      action: PayloadAction<ColumnType[]>,
    ) => {
      return action.payload
    },
    [addColumn.fulfilled.type]: (state, action: PayloadAction<ColumnType>) => {
      return [action.payload, ...state]
    },
  },
})

const {reducer, actions} = columnsSlice

export {reducer, actions}
