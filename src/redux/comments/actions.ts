import {createAsyncThunk} from '@reduxjs/toolkit'
import {getCommentsApi, addCommentApi} from './api'
import {COMMENT_ADD, COMMENTS_GET, ERROR_STRING} from '../../constants'
import {CommentType, ParamsType} from './types'

export const getComments = createAsyncThunk<CommentType>(
  COMMENTS_GET,
  async () => {
    const {data} = await getCommentsApi()
    if (data.message) {
      throw new Error(`${ERROR_STRING}: ${data.message}`)
    }
    return data
  },
)

export const addComment = createAsyncThunk<CommentType, ParamsType>(
  COMMENT_ADD,
  async params => {
    const {data} = await addCommentApi(params)
    if (data.message) {
      throw new Error(`${ERROR_STRING}: ${data.message}`)
    }
    return data
  },
)
