import React from 'react'
import {RootState} from '../../../../redux/store'
import {useSelector} from 'react-redux'
import {selectComment} from '../../../../redux/comments/selectors'
import {StyleSheet, Text, View} from 'react-native'
import {variables} from '../../../../assets/variables'

type Props = {
  id: number
}

export const PrayerComment = ({id}: Props) => {
  const body = useSelector((state: RootState) =>
    selectComment(state, id),
  )!.body

  return (
    <View style={styles.container}>
      <View style={styles.memberImageWrapper} />
      <View>
        <View style={styles.comment}>
          <Text style={styles.memberName}>Johny</Text>
          <Text style={styles.timePassed}>4 days ago</Text>
        </View>
        <Text style={styles.commentText}>{body}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: -1,
    width: '100%',
    height: 74,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderTopColor: variables.colors.defaultBorder,
    borderBottomColor: variables.colors.defaultBorder,
  },
  memberImageWrapper: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 9,
    marginBottom: 15,
    width: 46,
    height: 46,
    borderWidth: 1,
    borderColor: variables.colors.theme,
    borderRadius: 23,
  },
  comment: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    marginTop: 15,
  },
  memberName: {
    marginRight: 5,
    fontSize: variables.sizing.title,
    color: variables.colors.defaultBlack,
  },
  timePassed: {
    marginTop: 3,
    fontSize: 13,
    color: variables.colors.defaultLightGrey,
  },
  commentText: {
    marginTop: 2,
    fontSize: variables.sizing.title,
    color: variables.colors.defaultBlack,
  },
})
