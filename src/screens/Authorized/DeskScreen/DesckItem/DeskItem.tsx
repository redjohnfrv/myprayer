import React from 'react'
import {useSelector} from 'react-redux'
import {RootState} from '../../../../redux/store'
import {selectColumn} from '../../../../redux/columns/selectors'
import {useNavigation} from '@react-navigation/core'
import {ERoutes} from '../../../../constants'
import {StyleSheet, Text, TouchableOpacity} from 'react-native'
import {variables} from '../../../../assets/variables'

type Props = {
  id: number
}

export const DeskItem = ({id}: Props) => {
  const navigation = useNavigation()
  const {title} = useSelector((state: RootState) =>
    selectColumn(state, id),
  )!

  return (
    //@ts-ignore
    <TouchableOpacity style={styles.desk} onPress={() => navigation.navigate(ERoutes.ROUTE_PRAYER_LIST, {id})}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  desk: {
    marginRight: 15,
    marginBottom: 10,
    marginLeft: 15,
    borderWidth: 1,
    borderColor: variables.colors.defaultBorder,
    borderRadius: 4,
  },
  title: {
    lineHeight: 60,
    marginLeft: 15,
    fontSize: variables.sizing.title
  }
})
