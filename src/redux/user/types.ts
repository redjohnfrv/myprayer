export type ParamsType = {
  email: string
  password: string
  name: string
}

export type SignInParamsType = {
  email: string
  password: string
}

export type UserSignUpType = {
  email: string
  password: string
  name: string
}

export type UserSignInType = {
  email: string
  password: string
}

export type UserType = {
  email: string
  password: string
  name: string
  token: string
  columns: any[]
  id: number
  message: string
}
