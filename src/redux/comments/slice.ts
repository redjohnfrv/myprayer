import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {getComments, addComment} from './actions'
import {CommentType} from './types'

const initialState = [] as CommentType[]

const commentsSlice = createSlice({
  name: 'comments',
  initialState,
  reducers: {},
  extraReducers: {
    [getComments.fulfilled.type]: (
      state,
      action: PayloadAction<CommentType[]>,
    ) => {
      return action.payload
    },
    [addComment.fulfilled.type]: (state, action: PayloadAction<CommentType>) => {
      return [action.payload, ...state]
    },
  },
})

const {reducer, actions} = commentsSlice

export {reducer, actions}
