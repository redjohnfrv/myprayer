export type ColumnType = {
  id: number
  title: string
  description: null | string
  userId: number
  message: string
}

export type addColumnType = {
  title: string;
  description: string | null
}
