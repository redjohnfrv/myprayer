import {configureStore, getDefaultMiddleware} from '@reduxjs/toolkit'
import {rootReducer} from './rootReducer'
import {persistStore, persistReducer} from 'redux-persist'
import AsyncStorage from '@react-native-async-storage/async-storage'
import {authMiddleware} from './authMiddleware'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './rootSaga'

export type RootState = ReturnType<typeof rootReducer>

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['user'],
}

const pReducer = persistReducer(persistConfig, rootReducer)

const sagaMiddleware = createSagaMiddleware()
const middlewares = [sagaMiddleware, authMiddleware]

const store = configureStore({
  reducer: pReducer,
  middleware: [...getDefaultMiddleware({serializableCheck: false}), ...middlewares]
})

sagaMiddleware.run(rootSaga)

const persistor = persistStore(store)

export {store, persistor}
