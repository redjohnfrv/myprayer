import {MemberType} from './types'
import {createSlice} from '@reduxjs/toolkit'
import _ from 'lodash'



const initialState = [] as MemberType[]

const membersSlice = createSlice({
  name: 'members',
  initialState,
  reducers: {
    addMember: (state, action) => {
      state.push(action.payload)
    },
    removeAllMembers: (state) => {
      _.remove(state, item => item)
    }
  },
})

export default membersSlice.reducer
export const {addMember, removeAllMembers} = membersSlice.actions
