//api constants
export const API_URL = 'https://prayer.herokuapp.com/'
export const URL_PREFIX_COLUMNS = '/columns'
export const URL_PREFIX_PRAYERS = '/prayers'
export const URL_PREFIX_COMMENTS = '/comments'
export const URL_PREFIX_SIGN_UP = '/auth/sign-up'
export const URL_PREFIX_SIGN_IN = '/auth/sign-in'

//text constants
export const EMPTY_STRING = ''
export const ERROR_STRING = 'Error'

//redux actions constants
export const COLUMNS_GET = 'columns/getColumns'
export const COLUMN_ADD = 'columns/addColumn'
export const PRAYERS_GET = 'prayers/getPrayers'
export const PRAYER_ADD = 'prayers/addPrayer'
export const PRAYER_UPDATE = 'prayers/updatePrayer'
export const COMMENTS_GET = 'comments/getComments'
export const COMMENT_ADD = 'comments/addComment'
export const USER_SET = 'user/set'
export const USER_SIGN_UP = 'user/signUp'
export const USER_SIGN_IN = 'user/signIn'
export const MEMBERS_GET = 'members/getMembers'
export const MEMBER_ADD = 'members/addMember'
export const MEMBERS_UPDATE = 'members/updateMembers'


//route constants
export enum ERoutes {
  ROUTE_REGISTER = 'REGISTER',
  ROUTE_LOGIN = 'LOGIN',
  ROUTE_DESK = 'DESK',
  ROUTE_PRAYER_LIST = 'PRAYERS',
  ROUTE_PRAYER_INFO = 'PRAYER',
}

//OTHER
export const TAB_SWITCH_LEFT = 'left'
export const SHOW_ANSWERED = 'show answered prayers'
export const HIDE_ANSWERED = 'hide answered prayers'
export const TIMEOUT = 50000
