import React, {useState} from 'react'
import {RootState} from '../../../../redux/store'
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native'
import CheckBox from '@react-native-community/checkbox'
import {useNavigation} from '@react-navigation/core'
import {useSelector, useDispatch} from 'react-redux'
import {ERoutes} from '../../../../constants'
import {updatePrayer} from '../../../../redux/prayers/actions'
import {selectPrayer} from '../../../../redux/prayers/selectors'
import {images} from '../../../../assets/images'
import {variables} from '../../../../assets/variables'

type Props = {
  id: number
}

export const PrayerItem = ({id}: Props) => {
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const {
    title,
    description,
    checked
  } = useSelector((state: RootState) => selectPrayer(state, id))!
  const [toggleCheckBox, setToggleCheckBox] = useState(checked)

  return (
    <View style={styles.container}>
      <View style={styles.line}>
        <Image source={images.lineRed} />
      </View>
      <CheckBox
        boxType="square"
        onCheckColor={variables.colors.defaultBlack}
        onTintColor={variables.colors.defaultBlack}
        value={toggleCheckBox}
        onChange={() => {
          setToggleCheckBox(!toggleCheckBox)
          dispatch(
            updatePrayer({
              prayerId: id,
              title,
              description,
              checked: !toggleCheckBox,
            }),
          )
        }}
      />
      <TouchableOpacity
        //@ts-ignore
        onPress={() => navigation.navigate(ERoutes.ROUTE_PRAYER_INFO, {id})}
        style={styles.prayerTitle}>
        <Text
          //@ts-ignore
          style={lineThroughHandler(checked)}
        >
          {title}
        </Text>
      </TouchableOpacity>
      <Image source={images.people} />
      <View style={styles.count}>
        <Text>1</Text>
      </View>
      <Image source={images.prayBlue} />
      <View style={styles.count}>
        <Text>2</Text>
      </View>
    </View>
  )
}

function lineThroughHandler(checked: boolean) {
  let lineDecor
  checked
    ? lineDecor = 'line-through'
    : lineDecor = 'none'
  return {
    marginLeft: 15,
    fontSize: variables.sizing.title,
    lineHeight: 24,
    textDecorationLine: lineDecor,
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    width: 365,
    marginRight: 15,
    marginLeft: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 66,
    borderBottomWidth: 1,
    borderBottomColor: variables.colors.defaultBorder,
  },
  line: {
    width: 24,
    height: 24,
  },
  checkbox: {
    borderRadius: 0,
    borderWidth: 1,
    borderColor: '#fff',
  },
  prayerTitle: {
    flex: 1,
  },
  count: {
    width: 21,
    marginLeft: 5,
    fontSize: 12,
  }
})
