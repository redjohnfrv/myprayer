export type AddParamsType = {
  title: string
  description: string
  checked: boolean
  columnId: number
}

export type UpdateParamsType = {
  prayerId: number
  title: string
  description: string
  checked: boolean
}

export type AddPrayerType = {
  title: string
  description: string
  checked: boolean
  columnId: number
}

export type UpdatePrayerType = {
  prayerId: number
  title: string
  description: string
  checked: boolean
}

export type PrayerType = {
  id: number
  title: string
  description: string
  checked: boolean
  columnId: number
  commentsIds: number[]
  message: string
}
