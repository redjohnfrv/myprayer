import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {setUser, signIn, signUp} from './actions'
import {UserType} from './types'

const initialState = null as null | UserType

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: {
    [signUp.fulfilled.type]: (state, action: PayloadAction<UserType>) => {
      return action.payload
    },
    [signIn.fulfilled.type]: (state, action: PayloadAction<UserType>) => {
      return action.payload
    },
    [setUser.type]: (state, action: PayloadAction<any>) => {
      return action.payload
    },
  },
})

const {reducer, actions} = userSlice

export {reducer, actions}
