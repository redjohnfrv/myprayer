import React  from 'react'
import {
  Image,
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  FlatList
} from 'react-native'
import {RootState} from '../../../redux/store'
import {useNavigation} from '@react-navigation/core'
import {useSelector, useDispatch} from 'react-redux'
import {Form, Field} from 'react-final-form'
import {InputTransparent} from '../../../ui/InputTransparent'
import {PrayerMember} from './PrayerMember/PrayerMember'
import {PrayerComment} from './PrayerComment/PrayerComment'
import {addComment} from '../../../redux/comments/actions'
import {selectCommentsByPrayer} from '../../../redux/comments/selectors'
import {selectPrayer} from '../../../redux/prayers/selectors'
import {variables} from '../../../assets/variables'
import {images } from '../../../assets/images'
import {selectMembersByPrayer} from '../../../redux/members/selectors'
import {addMember, removeAllMembers} from '../../../redux/members/slice'
import { MEMBER_ADD } from "../../../constants";

type Props = {
  route: any
}

export const PrayerScreen = (props: Props) => {
  const {id} = props.route.params
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const {title} = useSelector((state: RootState) => selectPrayer(state, id))!
  const commentsIds = useSelector((state: RootState) =>
    selectCommentsByPrayer(state, id)
  )!
  const membersIds = useSelector((state: RootState) => selectMembersByPrayer(state, id))

  const onSubmitNewComment = async ({body}: {body: string}) => {
    try {
      await dispatch(addComment({body, prayerId: id}))
    } catch (error) {
      console.log(error)
    }
  }

  const onSubmitNewMember = () => {
    dispatch({type: MEMBER_ADD, payload: {id: id + Math.random() * 10_000, prayerId: id}})
  }

  const onSubmitRemoveAllMembers = () => {
    dispatch(removeAllMembers())
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={styles.arrow} onPress={() => navigation.goBack()}>
          <Image source={images.backArrowWhite} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.prayBtn}>
          <Image source={images.prayerLineWhite} />
        </TouchableOpacity>
        <View style={styles.title}>
          <Text style={styles.titleText}>{title}</Text>
        </View>
      </View>
        <Text style={styles.commentsTitle}>comments</Text>
          <FlatList
            style={styles.commentsList}
            ListHeaderComponent={
              <>
                <View style={styles.infoContainer}>
                  <View style={styles.infoHeader}>
                    <View style={styles.infoLine}>
                      <Image source={images.lineRed} />
                    </View>
                    <Text style={styles.lastPrayedText}>Last prayed 8 min ago</Text>
                  </View>
                  <View style={styles.infoBody}>
                    <View style={styles.infoFeatures}>
                      <View>
                        <Text style={styles.dateText}>July 25 2017</Text>
                      </View>
                      <View>
                        <Text style={styles.infoText}>Times Prayed By Others</Text>
                      </View>
                      <Text style={styles.infoSubText}>Opened for 4 days</Text>
                    </View>
                    <View style={styles.infoFeatures}>
                      <View>
                        <Text style={styles.countText}>123</Text>
                      </View>
                      <View>
                        <Text style={styles.infoText}>Times Prayed Total</Text>
                      </View>
                    </View>
                    <View style={styles.infoFeatures}>
                      <View>
                        <Text style={styles.countText}>123</Text>
                      </View>
                      <View>
                        <Text style={styles.infoText}>Times Prayed By Me</Text>
                      </View>
                    </View>
                    <View style={styles.infoFeatures}>
                      <View>
                        <Text style={styles.countText}>123</Text>
                      </View>
                      <View>
                        <Text style={styles.infoText}>Times Prayed By Others</Text>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.members}>
                  <Text style={styles.membersTitle}>members</Text>
                  <View style={styles.membersList}>
                    {membersIds?.map((item: number) => <PrayerMember key={item} id={item}/>)}
                    <TouchableOpacity
                      onPress={onSubmitNewMember}
                      style={styles.memberAdd}>
                      <Image source={images.addWhitePlus} />
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity style={styles.fire} onPress={onSubmitRemoveAllMembers}>
                    <Text style={styles.fireText}>Fire!</Text>
                  </TouchableOpacity>
                </View>
              </>
            }
            nestedScrollEnabled={true}
            keyExtractor={item => String(item)}
            data={commentsIds}
            renderItem={(item) => <PrayerComment id={item.item} />}
          />
      <View style={styles.footer}>
        <Form
          onSubmit={onSubmitNewComment}
          render={({handleSubmit}) => (
            <View style={styles.commentAdd}>
              <TouchableOpacity
                style={styles.commentAddBtn}
                onPress={handleSubmit}>
                <Image source={images.addComment} />
              </TouchableOpacity>
              {/*@ts-ignore*/}
              <Field component={InputTransparent}
                     name="body"
                     placeholder="Add a comment..."
              />
            </View>
          )}
        />
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    padding: 0,
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#FFF',
  },
  header: {
    width: '100%',
    height: 130,
    backgroundColor: variables.colors.theme
  },
  arrow: {
    position: 'absolute',
    top: 20,
    left: 15,
    zIndex: 10,
  },
  prayBtn: {
    position: 'absolute',
    top: 15,
    right: 15,
    width: 30,
    height: 30,
  },
  title: {
    marginTop: 50,
    paddingLeft: 10,
    paddingRight: 10,
  },
  titleText: {
    fontSize: variables.sizing.title,
    lineHeight: 27,
    color: '#FFF',
  },
  commentsTitle: {
    paddingTop: 15,
    paddingLeft: 10,
    fontSize: 13,
    color: variables.colors.defaultBlue,
    textTransform: 'uppercase',
  },
  commentsList: {
    marginTop: 15,
    marginBottom: 40,
    borderTopWidth: 1,
    borderTopColor: variables.colors.defaultBorder,
  },
  infoContainer: {
    width: '100%',
    height: 266,
  },
  infoHeader: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: 50,
  },
  infoLine: {
    marginLeft: 3,
    width: 25,
    height: 25,
    alignItems: 'center',
  },
  lastPrayedText: {
    fontSize: variables.sizing.title,
    color: variables.colors.defaultBlack,
  },
  infoBody: {
    width: '100%',
    height: 220,
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderBottomWidth: 1,
    borderBottomColor: variables.colors.defaultBorder,
  },
  infoFeatures: {
    width: '50%',
    height: 110,
    paddingLeft: 15,
    justifyContent: 'center',
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderColor: variables.colors.defaultBorder,
  },
  countText: {
    fontSize: 32,
    color: variables.colors.theme,
  },
  infoText: {
    fontSize: 13,
    color: variables.colors.defaultBlack,
  },
  dateText: {
    marginTop: 23,
    marginBottom: 8,
    fontSize: 22,
    color: variables.colors.theme,
  },
  infoSubText: {
    fontSize: 13,
    color: variables.colors.defaultBlue,
  },
  members: {
    position: 'relative',
    width: '100%',
    paddingTop: 20,
    paddingLeft: 15,
    marginBottom: 15,
  },
  membersTitle: {
    fontSize: 13,
    color: variables.colors.defaultBlue,
    textTransform: 'uppercase',
  },
  membersList: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    marginTop: 15,
  },
  memberAdd: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 32,
    height: 32,
    lineHeight: 32,
    backgroundColor: variables.colors.theme,
    borderRadius: 16,
  },
  footer: {
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
  },
  commentAdd: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  commentAddBtn: {
    width: 20,
    height: 20,
    marginRight: 12,
    marginTop: 5,
    marginLeft: 3,
  },
  fire: {
    position: 'absolute',
    top: 20,
    left: 85,
  },
  fireText: {
    fontSize: 13,
    fontStyle: 'italic',
    color: variables.colors.defaultRed,
  },
})
