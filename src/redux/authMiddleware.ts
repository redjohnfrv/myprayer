import {AnyAction, Dispatch} from 'redux'
import {REHYDRATE} from 'redux-persist'
import {globalApiHttp} from '../api/api'
import {signUp, signIn} from './user/actions'

export const authMiddleware = () => (next: Dispatch) => (action: AnyAction) => {
  switch (action.type) {
    case signUp.fulfilled.type:
      action.payload?.token
      && globalApiHttp.setAuthorizationHeader(action.payload.token)
      return next(action)
    case signIn.fulfilled.type:
      action.payload?.token
      && globalApiHttp.setAuthorizationHeader(action.payload.token)
      return next(action)
    case REHYDRATE:
      action.payload?.user?.token
      && globalApiHttp.setAuthorizationHeader(action.payload.user.token)
      return next(action)
    default: return next(action)
  }
}
