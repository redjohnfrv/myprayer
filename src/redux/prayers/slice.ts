import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {PrayerType} from './types'
import {addPrayer, getPrayers, updatePrayer} from './actions'

const initialState = [] as PrayerType[]

const prayersSlice = createSlice({
  name: 'prayers',
  initialState,
  reducers: {},
  extraReducers: {
    [getPrayers.fulfilled.type]: (
      state,
      action: PayloadAction<PrayerType[]>,
    ) => {
      return action.payload
    },
    [addPrayer.fulfilled.type]: (state, action: PayloadAction<PrayerType>) => {
      return [action.payload, ...state]
    },
    [updatePrayer.fulfilled.type]: (state, action: PayloadAction<PrayerType>) => {
      const newState = state.map(prayer => {
        if (prayer.id === action.payload.id) {
          return action.payload
        }
        return prayer
      })
      return newState
    },
  },
})

const {reducer, actions} = prayersSlice

export {reducer, actions}
