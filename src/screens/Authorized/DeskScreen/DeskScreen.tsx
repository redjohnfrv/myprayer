import React, {useEffect} from 'react'
import {
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  FlatList
} from 'react-native'
import {Form} from 'react-final-form'
import {DeskItem} from './DesckItem/DeskItem'
import {useSelector, useDispatch, shallowEqual} from 'react-redux'
import {selectColumns} from '../../../redux/columns/selectors'
import {setUser} from '../../../redux/user/actions'
import {getColumns, addColumn} from '../../../redux/columns/actions'
import {getComments} from '../../../redux/comments/actions'
import {getPrayers} from '../../../redux/prayers/actions'
import {EMPTY_STRING} from '../../../constants'
import {variables} from '../../../assets/variables'
import {images} from '../../../assets/images'

export const DeskScreen = () => {

  const columnsIds = useSelector(selectColumns, shallowEqual)
  const dispatch = useDispatch()

  const onSubmit = async ({title}: {title: string}) => {
    try {
      await dispatch(addColumn({title, description: EMPTY_STRING}))
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    dispatch(getColumns())
    dispatch(getPrayers())
    dispatch(getComments())
  }, [])

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <Form
          onSubmit={onSubmit}
          render={({handleSubmit}) => (
            <View style={styles.header}>
              <TouchableOpacity style={styles.closeBtn} onPress={() => dispatch(setUser(null))}>
                <Image source={images.closeBtn} />
              </TouchableOpacity>
              <View style={styles.titleWrapper}>
                <Text style={styles.title}>My Desk</Text>
              </View>
              <TouchableOpacity style={styles.addBtn} onPress={handleSubmit}>
                <Image source={images.addPlus} />
              </TouchableOpacity>
            </View>
          )}
        />
      </View>
      <View>
        <FlatList
          data={columnsIds}
          renderItem={(item) => <DeskItem id={item.item} />}
          keyExtractor={item => String(item)}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    marginTop: 50,
    backgroundColor: variables.colors.defaultBG,
    flex: 1,
    alignSelf: 'stretch',
  },
  wrapper: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: variables.colors.defaultBorder,
  },
  header: {
    height: 65,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  closeBtn: {
    position: 'absolute',
    top: 20,
    left: 20,
  },
  addBtn: {
    position: 'absolute',
    top: 20,
    left: 360,
  },
  titleWrapper: {
    paddingTop: 20,
  },
  title: {
    fontSize: variables.sizing.title,
  },
})
