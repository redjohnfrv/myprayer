import {createAsyncThunk} from '@reduxjs/toolkit'
import {
  getPrayersApi,
  addPrayerApi,
  updatePrayerApi,
} from './api'
import {
  ERROR_STRING,
  PRAYER_ADD,
  PRAYER_UPDATE,
  PRAYERS_GET
} from "../../constants"
import {AddParamsType, PrayerType, UpdateParamsType} from './types'

export const getPrayers = createAsyncThunk<PrayerType>(PRAYERS_GET, async () => {
  const {data} = await getPrayersApi()
  if (data.message) {
    throw new Error(ERROR_STRING)
  }
  return data
})

export const addPrayer = createAsyncThunk<PrayerType, AddParamsType>(
  PRAYER_ADD,
  async params => {
    const {data} = await addPrayerApi(params)
    if (data.message) {
      throw new Error(ERROR_STRING)
    }
    return data
  },
)

export const updatePrayer = createAsyncThunk<PrayerType, UpdateParamsType>(
  PRAYER_UPDATE,
  async params => {
    const {data} = await updatePrayerApi(params)
    if (data.message) {
      throw new Error(ERROR_STRING)
    }
    return data
  },
)
