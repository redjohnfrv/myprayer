export type ParamsType = {
  body: string
  prayerId: number
}

export type AddCommentType = {
  body: string
  prayerId: number
}

export type CommentType = {
  id: number
  body: string
  created: string
  prayerId: number | null
  userId: number
  message: string
}
