import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {Unauthorized} from './Unauthorized/Unauthorized'
import {Authorized} from './Authorized/Authorized'
import {useSelector} from 'react-redux'
import {selectUser} from '../redux/user/selectors'

export const HomeScreen = () => {
  const isAuthorized = useSelector(selectUser)

  return (
    <NavigationContainer>
      {isAuthorized ? <Authorized /> : <Unauthorized />}
    </NavigationContainer>
  )
}
