import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import {LoginScreen} from './LoginScreen/LoginScreen'
import {RegisterScreen} from './RegisterScreen/RegisterScreen'
import {ERoutes} from '../../constants'

export const Unauthorized = () => {

  const Stack = createStackNavigator()

  return (
    <Stack.Navigator initialRouteName={ERoutes.ROUTE_LOGIN}>
      <Stack.Screen
        name={ERoutes.ROUTE_LOGIN}
        component={LoginScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={ERoutes.ROUTE_REGISTER}
        component={RegisterScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  )
}
