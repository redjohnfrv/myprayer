import React from 'react'
import {Form, Field} from 'react-final-form'
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import {useNavigation} from '@react-navigation/core'
import {useDispatch} from 'react-redux'
import {signUp} from '../../../redux/user/actions'
import {Input} from '../../../ui/Input'
import {images} from '../../../assets/images'
import {variables } from '../../../assets/variables'

type ValuesType = {
  email: string
  login: string
  password: string
}

export const RegisterScreen = () => {
  const navigation = useNavigation()
  const dispatch = useDispatch()

  const onSubmit = async (values: ValuesType) => {
    const data = {
      email: values.email,
      password: values.password,
      name: values.login,
    }
    try {
      await dispatch(signUp(data))
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <TouchableOpacity style={styles.arrow} onPress={() => navigation.goBack()}>
        <Image style={styles.arrowImg} source={images.backArrow} />
      </TouchableOpacity>
      <Text>Please register</Text>
      <Form
        onSubmit={onSubmit}
        render={({handleSubmit}) => (
          <View>
              {/*@ts-ignore*/}
              <Field component={Input}
                     name="email"
                     placeholder="Your email ..."
              />
              {/*@ts-ignore*/}
              <Field component={Input}
                     name="login"
                     placeholder="Your name ..."
              />
              {/*@ts-ignore*/}
              <Field component={Input}
                     secure
                     name="password"
                     placeholder="Your password ..."
              />

            <TouchableOpacity style={styles.button} onPress={handleSubmit}>
              <Text>CONFIRM</Text>
            </TouchableOpacity>
          </View>
        )}
      />
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    height: '100%',
    backgroundColor: variables.colors.theme,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: 209,
    height: 30,
    marginBottom: 10,
    backgroundColor: variables.colors.defaultBG,
    color: variables.colors.defaultBlack,
    borderWidth: 1,
    borderColor: variables.colors.defaultBlack,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  arrow: {
    position: 'absolute',
    top: 70,
    left: 20,
  },
  arrowImg: {
    width: 20,
    height: 20,
  }
})

