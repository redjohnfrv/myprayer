import {combineReducers} from 'redux'
import {userReducer} from './user'
import {columnsReducer} from './columns'
import {prayersReducer} from './prayers'
import {commentsReducer} from './comments'
import {membersReducer} from './members'

const rootReducer = combineReducers({
  user: userReducer,
  columns: columnsReducer,
  prayers: prayersReducer,
  comments: commentsReducer,
  members: membersReducer,
})

export {rootReducer}
