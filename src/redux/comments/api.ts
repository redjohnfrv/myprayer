import {AxiosPromise} from 'axios'
import {globalApiHttp} from '../../api/api'
import {CommentType, AddCommentType} from './types'
import {URL_PREFIX_COMMENTS} from '../../constants'

export const getCommentsApi = (): AxiosPromise<CommentType> => {
  return globalApiHttp.get(URL_PREFIX_COMMENTS)
}

export const addCommentApi = (payload: AddCommentType): AxiosPromise<CommentType> => {
  const {prayerId, ...body} = payload
  return globalApiHttp.post(`/prayers/${String(prayerId)}/comments`, body)
}


