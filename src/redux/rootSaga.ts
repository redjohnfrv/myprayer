import {all} from 'redux-saga/effects'
import membersWatcher from './members/sagas'

export default function* () {
  yield all([
    membersWatcher(),
  ])
}
